# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


def generate_email_address(first_name: str, last_name: str) -> str:
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"


def calculation_of_sum_grades(list_of_grades: list) -> float:
    return sum(list_of_grades) / len(list_of_grades)


if __name__ == '__main__':
    print_hi('PyCharm')

# Examples

email = ["email1@gmail.com", "email2@gmail.com", "email3@gmail.com", "email4@gmail.com"]
print(f"Length of emails list is: {len(email)}")
print(f"First element of email list is: {email[0]}")
print(f"Last element of email list is: {email[-1]}")
email.append("email_example@gmail.com")
print(f"Length of emails list is: {len(email)}")
print(f"List of ne email list is: {email}")

email_janusz = generate_email_address("Janusz", "KOWALSKI")
email_tadeusz = generate_email_address("TADEUSZ", "JuzWIAK")
print(f"First generated email: {email_janusz}\n")
print(f"Second generated email: {email_tadeusz}\n")

student = (1, 2, 5, 6.5, 3, 5, 3, 2)
calc_student = calculation_of_sum_grades(student)
print(f"Sum of student grades: {calc_student}")
