pet = {
    'name': 'Burek',
    'kind': 'Dog',
    'age': '10',
    'weight': '6.5',
    'male': True,
    'favorite_food': ['chicken', 'fish']
}

print(pet['weight'])
pet['weight'] = 8.8
pet["like_swimming"] = False
del pet["male"]
pet['favorite_food'].append('snack')
print(pet)


def set_person_data(email, phone, city, street):
    return {
        'contact': {
            'email': email,
            'phone': phone
        },
        'address': {
            'city': city,
            'street': street
        }
    }
