def get_three_numbers_in_the_list(list_of_numbers):
    sorted_list = sorted(list_of_numbers)
    return sorted_list[-3:]


shopping_list = ["oranges", "water", "chicken", "potatoes", "washing_liquid"]
print(shopping_list[0])
print(shopping_list[1], [6])
print(shopping_list[2])
shopping_list.append('lemons')
print(shopping_list)
print(shopping_list[-1])
number_of_item_to_buy = len(shopping_list)
print(number_of_item_to_buy)

first_three_items = shopping_list[0:3]
print(first_three_items)

animal = {
    'name': 'Burek',
    'kind': 'Dog',
    'age': '10',
    'male': True
}

dog_age = animal['age']
print(dog_age)
dog_male = animal['male']
print(dog_male)

movies = ["Dune", "Blade", "Solaris", "Star Wars", "Her"]
print(len(movies))
print(movies[0])
last_movie_index = len(movies) - 1
print(movies[-1])
movies.append("Duo")
movies.insert(2, "Quake")
print(len(movies))


print(get_three_numbers_in_the_list([1, -2, 7, -2, 1, 3]))
