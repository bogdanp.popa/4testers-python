def example_of_email_list(email):
    print("First element of email is", email[0])
    print("Last element of email is", email[-1])
    print("Current list is ", email)


if __name__ == '__main__':
    example_of_email_list(["exampl1@gmail.com", "example2@gmail.com"])
