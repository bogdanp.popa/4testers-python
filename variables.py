friends_name = "Kamil"
friends_age = 40
number_of_animals = 2
has_driving_license = True
friendship_year = 20.5

print("Years", friendship_year, sep="\t")
print("Name", friends_name)
print("Pet", number_of_animals)
print("License", has_driving_license)
print("Age", friends_age)

print("Name", friends_name, "Years", friends_age)

name = "Popa Bogdan"
email = "bogdanp.popa@gmaiil.com"
phone_number = "780150303"

bio = name + email + phone_number
# f" jesli prze apostrofami umiescimy f to w klamrach mozemy umiescic wartosci - f string tak to sie nazywa
print(bio)
print("-----------------")
bio_smarter = f"Name: {name}\nemail: {email}\nPhone: {phone_number}"
print(bio_smarter)

